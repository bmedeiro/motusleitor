package programac.revistamotus.model;

/**
 * Created by Bruno on 1/8/2018.
 */

public class Motus1 {

  String equipe =
    "MOTUS - Movimento Literário Digital #1\n\n"+
    "Publicada em 06/2017\n\n"+
    "Equipe Executora:\n"+
            "Aline Vieira de Mello\n"+
            "Bruno Medeiros\n"+
            "Luana Cadaval\n"+
            "Eduardo Serpa\n"+
            "Sarah Ruth\n"+
            "Giovane Mendonça\n"+
            "Mário Lima\n"+
            "Eric Oliveira\n"+
            "Amanda Lopes\n"+
            "Jéssica Brasil\n"+
            "Kézia Lôbo\n"+
            "Cristiane Fontoura\n"+
            "Manoel Henrique\n\n"+

            "Equipe Julgadora:\n"+
            "Paulo Berquó\n"+
            "Marileia Marchezan\n"+
            "Alexandre Alderete\n"+
            "Pedro do Amaral\n"+
            "Lucelia Martins\n"+
            "Rosa Helena Martinez\n";
   String editorial =
    "\n"+
    "Movimentar a Cultura de uma nação não é uma tarefa simples. Contudo, as dificuldades"+
    " enfrentadas durante a criação da Motus não esmoreceram os integrantes."+
    " A caminhada não foi curta. Há mais de um ano a primeira ideia do que hoje chamamos de"+
    " Motus - Movimento Literário Digital - surgiu. Desde então houve dezenas de reuniões,"+
    " centenas de emails e muito (muito mesmo!) esforço coletivo."+
    "\n\nDebatemos, refletimos, nos apoiamos e nos erguemos. Hoje, trinta de junho de dois mil e"+
    " dezessete, aqui estamos."+
    "\nNa Motus #1, reunimos 11 poemas e 9 contos sobre o tema Liberdade de Expressão."+
    "\nSão 20 escritores brasileiros de diferentes estados que nos"+
    " prestigiaram com a escrita de suas obras e a confiança de seus trabalhos na nossa ação de"+
    " extensão."+
    "\n\nDesejamos uma leitura energética e crítica. Nenhuma verdade absoluta está escrita nas"+
    " páginas que virão. Nossa proposta é simples e lídima: que a literatura mova teus"+
    " pensamentos."+
    "\n\nSiga sempre em movimento!";

String editor = "\nBruno Medeiros // Editor\n";
String texto1 =
    "Na tevê, misérias\n"+
    "nos jornais,\n"+
    "meios banais\n"+
    "de noticiar a morte\n"+
    "desta vida encarcerada\n"+
    "de caras enceradas\n"+
    "pelo matiz da mentira\n" +
    "\n" +
    "Nos lides e lidas\n" +
    "muitas lambidas\n" +
    "muitos gemidos\n" +
    "espremidos em tinta\n" +
    "papel jornal,\n" +
    "letras, palavras jogral\n" +
    "em golpe germinal\n" +
    "\n" +
    "Das novas verdades\n" +
    "às antigas mentiras\n" +
    "novos formatos\n" +
    "para antigas máximas\n" +
    "em mínimas medidas\n" +
    "o que resta é o sentido\n" +
    "há muito invertido\n" +
    "\n" +
    "Das folhas brancas\n" +
    "encardidas matérias\n" +
    "nada etéreas\n" +
    "nada profundas\n" +
    "conteúdo incontido\n" +
    "em doses cavalares\n" +
    "de canalhice explícita\n" +
    "\n" +
    "Abriu-se, fechou-se\n" +
    "o esgoto escorre\n" +
    "palavras degradadas\n" +
    "letras mortas\n" +
    "idioma corrompido\n" +
    "verdade agredida\n" +
    "informação assassinada.\n";

String biografiaTexto1 =
    "Formado em Ciências Econômicas, especialização em Marketing, Recursos Humanos e Psicopedagogia Institucional."  +
    "Trabalha como profissional de marketing. É marketeiro por opção e escreve por paixão." +
    "Já teve textos selecionados para coletâneas de contos em nível nacional, como \"O último gargalo de Gaia\" e \"O fim vem das estrelas\". Também possui e-books na plataforma digital Sweek." +
    "A escrita, mais do um hobby é uma devoção.\n";

String nomeAutor1=
    "Paulo   Florindo\n";

String texto2 =
    "\n" +
    "Quisera poder voar sobre as nuvens\n" +
    "E alcançar o universo\n" +
    "Quisera eu ser tudo aquilo\n" +
    "Que meu coração almeja ser.\n" +
    "\n" +
    "Quisera eu ser livre\n" +
    "E compor uma melodia\n" +
    "Para essa tal liberdade.\n" +
    "\n" +
    "Quisera poder voar sobre as nuvens,\n" +
    "E alcançar o universo.\n" +
    "Quisera eu ser tudo aquilo\n" +
    "Que meu coração almeja ser.\n" +
    "\n" +
    "Quisera eu ser livre,\n" +
    "Para compor a própria canção\n" +
    "Do meu cansado coração.\n" +
    "\n" +
    "Ouço homens falando em liberdade\n" +
    "Mas onde está essa tal liberdade,\n" +
    "Se nossa expressão é silenciada\n" +
    "E nossa alma cala?\n" +
    "\n" +
    "Onde está a liberdade da rua\n" +
    "Dos becos e vielas?\n" +
    "Onde está o porto das verdades,\n" +
    "Que os navegantes viajam\n" +
    "Até chegar ao cais das palavras,\n" +
    "Submersas no mar da alma?\n" +
    "\n" +
    "A opinião grita\n" +
    "E os pensamentos clamam.\n" +
    "Deixemos que o mundo conheça,\n" +
    "O poder das ideias que ressurgem\n" +
    "Ao fim do crepúsculo,\n" +
    "Que brotam a luz da aurora.\n" +
    "\n" +
    "Vamos planar sobre o céu\n" +
    "Da nossa profunda expressão,\n" +
    "E deixar que as águas da liberdade\n" +
    "Alcancem a muralha,\n" +
    "Da indiferença humana." +
    "\n";

    String nomeAutor2=
    "Erivania Fernandes\n";

    String biografiaTexto2 =
    "Erivania dos santos Fernandes, pseudônimo Luna Mia, é uma escritora e poeta brasileira." +
    "Erivania nasceu em Feira de Santana, Bahia, no dia 17 de novembro de 1992." +
    "Filha de Erivaldo José Fernandes e Maria Domingas Araújo dos Santos Fernandes, sua primeira poesia publicada foi no ano de 2017, obra intitulada como fascínio, que foi destaque de um concurso realizado em sua cidade natal.\n";

    String texto3 =
                    "\nDoma tua língua! Guarda-a bem contigo!\n" +
                    "Não te enganes a ti com o que diz,\n" +
                    "Pois dos órgãos todos é a meretriz,\n" +
                    "Diacho indefesso não nos seja amigo!\n\n" +
                    "Do homem o mais maléfico inimigo!\n" +
                    "Cuide não crê-la; pois mais de ti diz,\n" +
                    "Quando doutros fala. É bem mais feliz\n" +
                    "Quem mais se cala. Pois foge o castigo,\n\n" +
                    "Se te encantas pela sabedoria.\n" +
                    "Reflita teu dito e estejas atento.\n" +
                    "Morem tuas posses na filosofia,\n\n" +
                    "E nela, somente, tenhas teu alento!\n" +
                    "Nova triste de ti não venha um dia:\n" +
                    "Jaz jus a língua – envenenamento.\n ";
    String nomeAutor3 = "Felipe Couto Lima\n";
    String biografiaTexto3 =
                    "Felipe Couto Lima tem trinta anos, assite em Rio de Janeiro, é formado em teologia" +
                    " pela FAECAD, morou e estudou no Canadá por seis meses, onde concluiu seus" +
                    " estudos de língua inglesa pela Vancouver English Center(VEC). Aficionado por" +
                    " línguas estrangeiras estudou, por conta própria, de francês a tailandês, possuindo" +
                    " também poemas na língua inglesa, como também na língua francesa. Crê que a" +
                    " poesia é mais que estética, mais que sentido, até mesmo mais que forma; vê a" +
                    " poesia como epifania do que de divino contém a nossa alma.";

    String texto4 =
                    "\nQuisera eu caber apenas dentro desta garrafa,\n" +
                    "moldar-me à sua forma e sentir-me realizada." +
                    "\n\n" +
                    "Quisera eu reprimir minha alma para aceitar sua imposição,\n" +
                    "encolher minhas verdades para inebriar-me com a suas vaidades!" +
                    "\n\n" +
                    "Quisera eu conter meus desprendimentos para deleitar-me com as suas ambições,\n" +
                    "compactar minha pureza para saborear sua lascívia,\n" +
                    "contrair os meus tesouros para degustar suas futilidades.\n" +
                    "\n\n" +
                    "Quisera eu comprimir minhas ponderações para me embriagar com seus impulsos,\n" +
                    "sintetizar meu equilíbrio para saborear seus exageros.\n" +
                    "\n" +
                    "Quisera eu caber apenas dentro desta garrafa!\n" +
                    "Meu mundo seria então simples e finito.\n" +
                    "E eu não teria mais motivos para escrever.\n";
    String nomeAutor4 = "Julieta de Souza\n";
    String biografiaTexto4=
                    "Julieta de Souza é divinopolitana, nasceu dia 13 de julho de 1972. Professora de" +
                    " Língua Portuguesa e Arte, ministra aulas no Ensino Fundamental e cursos de" +
                    " produções textuais para professores. É psicopedagoga, amante da literatura, da" +
                    " natureza e das pessoas, pratica yoga. É feliz cuidando da casa, criando mandalas e" +
                    " dançando. Cultiva quaresmeiras e girassóis no jardim e no coração.";

    String pagina3 = "#3";
    String pagina4 = "#4";
    String pagina6 = "#6";
    String pagina7 = "#7";
    String pagina8 = "#8";
    String pagina9 = "#9";
    String pagina10 = "#10";
    String pagina11 = "#11";
    String pagina12 = "#12";
    String pagina13 = "#13";

    public String getEquipe() {
        return equipe;
    }

    public String getEditorial() {
        return editorial;
    }

    public String getEditor() {
        return editor;
    }

    public String getTexto1() {
        return texto1;
    }

    public String getBiografiaTexto1() {
        return biografiaTexto1;
    }

    public String getNomeAutor1() {
        return nomeAutor1;
    }

    public String getTexto2() {
        return texto2;
    }

    public String getNomeAutor2() {
        return nomeAutor2;
    }

    public String getBiografiaTexto2() {
        return biografiaTexto2;
    }

    public String getPagina6() {
        return pagina6;
    }

    public String getPagina7() {
        return pagina7;
    }

    public String getPagina8() {
        return pagina8;
    }

    public String getPagina9() {
        return pagina9;
    }

    public String getPagina3() {
        return pagina3;
    }

    public String getPagina4() {
        return pagina4;
    }

    public String getTexto3() {
        return texto3;
    }

    public String getNomeAutor3() {
        return nomeAutor3;
    }

    public String getBiografiaTexto3() {
        return biografiaTexto3;
    }

    public String getPagina10() {
        return pagina10;
    }

    public String getPagina11() {
        return pagina11;
    }

    public String getTexto4() {
        return texto4;
    }

    public String getNomeAutor4() {
        return nomeAutor4;
    }

    public String getBiografiaTexto4() {
        return biografiaTexto4;
    }

    public String getPagina12() {
        return pagina12;
    }

    public String getPagina13() {
        return pagina13;
    }
}



