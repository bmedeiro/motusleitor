package programac.revistamotus.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import programac.revistamotus.R;


public class Motus_Tab extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private static int size;
    TextView toolbarTitulo;
    ImageButton settings;

    SharedPreferences aux;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getIntent().getExtras();

        aux =  getApplicationContext().getSharedPreferences("MyPref", 0);
        int auxx = aux.getInt("p",0);

        if(bundle != null) {
            size = auxx;
        }

        setContentView(R.layout.activity_motus_tab);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.barra));


        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/master_of_break.ttf");
        toolbarTitulo = (TextView) toolbar.findViewById(R.id.app_toolbar_title);
        toolbarTitulo.setTypeface(p2);
        toolbarTitulo.setText("motus");
        toolbarTitulo.setTextSize(20);
        toolbarTitulo.setTextColor(getResources().getColor(R.color.preto));

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container1);
        mViewPager.setAdapter(mSectionsPagerAdapter);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }



    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tab, container, false);

            return rootView;
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent i = new Intent(this, Configuracao.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Método que faz a divisão das opções do menu, a primeira divisão é relacionada ao fragmento processo, a segunda, ao fragmento andamento
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    Motus1_Pagina1 p1 = new Motus1_Pagina1();
                    Bundle b0 = new Bundle();
                    b0.putInt("size", size);
                    p1.setArguments(b0);
                    return p1;

                case 1:
                    Motus1_Pagina2 p2 = new Motus1_Pagina2();
                    Bundle b2 = new Bundle();
                    b2.putInt("size", size);
                    p2.setArguments(b2);
                    return p2;

                case 2:
                    Motus1_Pagina3 p3 = new Motus1_Pagina3();
                    Bundle b3 = new Bundle();
                    b3.putInt("size", size);
                    p3.setArguments(b3);
                    return p3;

                case 3:
                    Motus1_Pagina4 p4 = new Motus1_Pagina4();
                    Bundle b4 = new Bundle();
                    b4.putInt("size", size);
                    p4.setArguments(b4);
                    return p4;

                case 4:
                    Motus1_Pagina5 p5 = new Motus1_Pagina5();
                    Bundle b5 = new Bundle();
                    b5.putInt("size", size);
                    p5.setArguments(b5);
                    return p5;

                case 5:
                  Motus1_Pagina6 p6 = new Motus1_Pagina6();
                    Bundle b6 = new Bundle();
                    b6.putInt("size", size);
                    p6.setArguments(b6);
                    return p6;

                case 6:
                    Motus1_Pagina7 p7 = new Motus1_Pagina7();
                    Bundle b7 = new Bundle();
                    b7.putInt("size", size);
                    p7.setArguments(b7);
                    return p7;

                case 7:
                    Motus1_Pagina8 p8 = new Motus1_Pagina8();
                    Bundle b8 = new Bundle();
                    b8.putInt("size", size);
                    p8.setArguments(b8);
                    return p8;

                case 8:
                    Motus1_Pagina9 p9 = new Motus1_Pagina9();
                    Bundle b9 = new Bundle();
                    b9.putInt("size", size);
                    p9.setArguments(b9);
                    return p9;

                case 9:
                    Motus1_Pagina10 p10 = new Motus1_Pagina10();
                    Bundle b10 = new Bundle();
                    b10.putInt("size", size);
                    p10.setArguments(b10);
                    return p10;

                case 10:
                    Motus1_Pagina11 p11 = new Motus1_Pagina11();
                    Bundle b11 = new Bundle();
                    b11.putInt("size", size);
                    p11.setArguments(b11);
                    return p11;
                case 11:
                    Motus1_Pagina12 p12 = new Motus1_Pagina12();
                    Bundle b12 = new Bundle();
                    b12.putInt("size", size);
                    p12.setArguments(b12);
                    return p12;
                case 12:
                    Motus1_Pagina13 p13 = new Motus1_Pagina13();
                    Bundle b13 = new Bundle();
                    b13.putInt("size", size);
                    p13.setArguments(b13);
                    return p13;
            }
            return null;
        }

        /**
         * Método que retorna a contagem de seções
         *
         * @return
         */
        @Override
        public int getCount() {
            return 13;
        }

        /**
         * Método que faz a criação do título das seções
         *
         * @param position
         * @return
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Processo";
                case 1:
                    return "Andamento";

            }
            return null;
        }

    }

    /*public int mudarFonte(){
        int s = bundle.getInt("tamanhoFonte");
        int size;
        if (s==22){
            size = 22;
        }else if (s==20){
            size = 20;
        }else{
            size = 18;
        }
        return size;
    }


    public void kkk(View view) {
        mudarFonte();
    }*/

    private void enviarFonte (Intent i) {
        i.putExtra("s", 30);
    }


}