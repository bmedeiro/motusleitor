package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.john.waveview.WaveView;

import programac.revistamotus.R;
import programac.revistamotus.model.Motus1;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina8 extends Fragment {

    public View rootView;
    RelativeLayout layoutPagina6;
    TextView pagina;
    ImageView im;
    WaveView wave;
    Bundle bundle;
    int size=18;
    Motus1 m1 = new Motus1();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.pagina6, container, false);
        super.onCreate(savedInstanceState);

        bundle = getArguments();
        if (bundle.getInt("size") != 0){
            size = bundle.getInt("size");
        }else {
            size = 18;
        }

        layoutPagina6 = (RelativeLayout) rootView.findViewById(R.id.layoutPagina6);
        layoutPagina6.setBackgroundColor(getResources().getColor(R.color.vermelhoEmarrom));

        pagina = (TextView) rootView.findViewById(R.id.pagina6);
        Typeface p2 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/master_of_break.ttf");
        pagina.setTypeface(p2);
        pagina.setText(m1.getPagina8()+ "  ");
        pagina.setTextColor(getResources().getColor(R.color.preto));
        pagina.setTextSize(size);

        im = (ImageView) rootView.findViewById(R.id.DesenhoObra);
        im.setImageDrawable(getResources().getDrawable(R.drawable.pagina8));

        ScrollView scroll = (ScrollView) rootView.findViewById(R.id.scrollImagem);
        scroll.setBackgroundColor(getResources().getColor(R.color.vermelhoEmarrom));
        return rootView;
    }
}
