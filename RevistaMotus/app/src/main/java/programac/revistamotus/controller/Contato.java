package programac.revistamotus.controller;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import programac.revistamotus.R;

public class Contato extends AppCompatActivity {
    TextView tituloPagina, textoEmail, legendaButton;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contato);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarContato);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif-Italic.ttf");
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif.ttf");
        Typeface p3 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif-Bold.ttf");




        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new NavigationView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),  Configuracao.class);
                startActivity(i);
            }
        });




        tituloPagina= (TextView) findViewById(R.id.TituloPagina);
        tituloPagina.setText(" CONTATO");
        tituloPagina.setTextColor(getResources().getColor(R.color.almostBlack));
        tituloPagina.setTypeface(p3);
        tituloPagina.setTextSize(22);


        textoEmail = (TextView) findViewById(R.id.introEmail);
        textoEmail.setText("Possui alguma reclamação, sugestão ou elogio? \n"+ "Nos envie um email! \nRetonaremos o mais rápido possível.");
        textoEmail.setTypeface(p1);
        textoEmail.setTextSize(18);

        legendaButton= (Button) findViewById(R.id.legendaButton);
        legendaButton.setText("ENVIAR E-MAIL");
        legendaButton.setTypeface(p2);
        legendaButton.setTextColor(getResources().getColor(R.color.almostWhite));
        legendaButton.setTextSize(12);
        legendaButton.setOnTouchListener(new ButtonCustomizado());



    }


    private void enviaEmail(){
        Log.i("Send email", "");
        Intent emailIntent = new Intent (Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        String uriText =
                "mailto:motus.unipampa@gmail.com" +
                        "?subject=" + Uri.encode("Assunto") +
                        "&body=" + Uri.encode("Reclamação/Elogio/Sugestão?"+ "\n"+"\n"+" Conte-nos!"+"\n"+"Queremos sempre melhorar nosso serviço.");

        Uri uri = Uri.parse(uriText);
        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
        sendIntent.setData(uri);
        try {
            startActivity(Intent.createChooser(sendIntent, "Enviar email"));
            finish();
            Log.i("Enviando..", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Contato.this,
                    "Não há aplicativos de e-mail instalado em seu dispositivo.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.action_settings) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public void envialEmail(View view) {
        enviaEmail();
    }

}
