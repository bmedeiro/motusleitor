package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import programac.revistamotus.R;
import programac.revistamotus.model.Motus1;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina4 extends Fragment {

    public View rootView;
    TextView editorial, textoeditorial, editor, cargo;
    TextView pagina;
    Bundle bundle;
    int size=18;
    Motus1 m1 = new Motus1();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.editorial, container, false);
        super.onCreate(savedInstanceState);

        bundle = getArguments();
        if (bundle.getInt("size") != 0){
            size = bundle.getInt("size");
        }else {
            size = 18;
        }

        editorial= (TextView) rootView.findViewById(R.id.motusEditorial);
        Typeface p2 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/master_of_break.ttf");
        editorial.setTypeface(p2);


        textoeditorial= (JustifiedTextView) rootView.findViewById(R.id.textoeditorial);
        Typeface p1 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/DroidSerif-Bold.ttf");
      //  Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif.ttf");

        textoeditorial.setTypeface(p1);
        textoeditorial.setText(m1.getEditorial());
        textoeditorial.setTextSize(size);

        pagina = (TextView) rootView.findViewById(R.id.pagina4);
        pagina.setTypeface(p2);
        pagina.setText(m1.getPagina4() + "  ");
        pagina.setTextSize(size);
        pagina.setTextColor(getResources().getColor(R.color.branco));

        editor = (TextView) rootView.findViewById(R.id.editor);
        editor.setTypeface(p1);
        editor.setText(m1.getEditor());
        editor.setTextSize(size);

        return rootView;
    }
}
