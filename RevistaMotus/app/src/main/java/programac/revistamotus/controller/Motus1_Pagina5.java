package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import programac.revistamotus.R;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina5 extends Fragment {

    public View rootView;
    TextView sumario, cabeçalho, slogan;
    TextView obra1, autorObra1, numeroObra1,
            obra2, autorObra2, numeroObra2,
            obra3, autorObra3, numeroObra3,
            obra4, numeroObra4, autorObra4,
            obra5, numeroObra5, autorObra5,
            obra6, numeroObra6, autorObra6,
            obra7, numeroObra7, autorObra7,
            obra8, numeroObra8, autorObra8;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sumario, container, false);
        super.onCreate(savedInstanceState);

        Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Light.ttf");
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/master_of_break.ttf");

        sumario = (TextView) rootView.findViewById(R.id.text2);
        sumario.setTextSize(50);
        sumario.setText("motus");
        sumario.setTypeface(p2);
        sumario.setTextColor(rootView.getResources().getColor(R.color.branco));


        cabeçalho = (TextView) rootView.findViewById(R.id.text1);
        cabeçalho.setTextSize(18);
        cabeçalho.setText("NESTA EDIÇÃO DA MOTUS");
        cabeçalho.setTypeface(p1);
        cabeçalho.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));

        slogan = (TextView) rootView.findViewById(R.id.text3);
        slogan.setTextSize(15);
        slogan.setText("MOVIMENTO LITERÁRIO DIGITAL");
        slogan.setTypeface(p1);
        slogan.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));

        exibir();
        return rootView;

    }

    public void exibir(){

        Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Bold.ttf");
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Light.ttf");


        obra1 = (TextView) rootView.findViewById(R.id.obra1);
        obra1.setText("Flores Amarelas, esgoto letrado");
        obra1.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra1.setTypeface(p1);
        obra1.setTextSize(18);
       /* obra1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Motus1_Pagina7 p7 = new Motus1_Pagina7();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.container2, p7);
                fragmentTransaction.commit();            }
        });*/

        autorObra1 = (TextView) rootView.findViewById(R.id.autorObra1);
        autorObra1.setText("Paulo Florindo");
        autorObra1.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra1.setTypeface(p2);
        autorObra1.setTextSize(15);

        numeroObra1 = (TextView) rootView.findViewById(R.id.numeroObra1);
        numeroObra1.setText("7");
        numeroObra1.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra1.setTypeface(p1);
        numeroObra1.setTextSize(20);

        obra2 = (TextView) rootView.findViewById(R.id.obra2);
        obra2.setText("Essa tal liberdade");
        obra2.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra2.setTypeface(p1);
        obra2.setTextSize(18);


        autorObra2 = (TextView) rootView.findViewById(R.id.autorObra2);
        autorObra2.setText("Erivania Fernandes");
        autorObra2.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra2.setTypeface(p2);
        autorObra2.setTextSize(15);

        numeroObra2 = (TextView) rootView.findViewById(R.id.numeroObra2);
        numeroObra2.setText("9");
        numeroObra2.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra2.setTypeface(p1);
        numeroObra2.setTextSize(20);

        obra3 = (TextView) rootView.findViewById(R.id.obra3);
        obra3.setText("Doma tua língua!");
        obra3.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra3.setTypeface(p1);
        obra3.setTextSize(18);


        autorObra3 = (TextView) rootView.findViewById(R.id.autorObra3);
        autorObra3.setText("Felipe Lima");
        autorObra3.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra3.setTypeface(p2);
        autorObra3.setTextSize(15);

        numeroObra3 = (TextView) rootView.findViewById(R.id.numeroObra3);
        numeroObra3.setText("12");
        numeroObra3.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra3.setTypeface(p1);
        numeroObra3.setTextSize(20);

        obra4 = (TextView) rootView.findViewById(R.id.obra4);
        obra4.setText("Engarrafar-me?");
        obra4.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra4.setTypeface(p1);
        obra4.setTextSize(18);


        autorObra4 = (TextView) rootView.findViewById(R.id.autorObra4);
        autorObra4.setText("Julieta de Souza");
        autorObra4.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra4.setTypeface(p2);
        autorObra4.setTextSize(15);

        numeroObra4 = (TextView) rootView.findViewById(R.id.numeroObra4);
        numeroObra4.setText("14");
        numeroObra4.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra4.setTypeface(p1);
        numeroObra4.setTextSize(20);



        obra5 = (TextView) rootView.findViewById(R.id.obra5);
        obra5.setText("Reflexos");
        obra5.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra5.setTypeface(p1);
        obra5.setTextSize(18);


        autorObra5 = (TextView) rootView.findViewById(R.id.autorObra5);
        autorObra5.setText("Deise Oliveira S.");
        autorObra5.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra5.setTypeface(p2);
        autorObra5.setTextSize(15);

        numeroObra5 = (TextView) rootView.findViewById(R.id.numeroObra5);
        numeroObra5.setText("16");
        numeroObra5.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra5.setTypeface(p1);
        numeroObra5.setTextSize(20);

        obra6 = (TextView) rootView.findViewById(R.id.obra6);
        obra6.setText("Liberdade de Expressão");
        obra6.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra6.setTypeface(p1);
        obra6.setTextSize(18);


        autorObra6 = (TextView) rootView.findViewById(R.id.autorObra6);
        autorObra6.setText("Fábio Silva");
        autorObra6.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra6.setTypeface(p2);
        autorObra6.setTextSize(15);

        numeroObra6 = (TextView) rootView.findViewById(R.id.numeroObra6);
        numeroObra6.setText("18");
        numeroObra6.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra6.setTypeface(p1);
        numeroObra6.setTextSize(20);


        obra7 = (TextView) rootView.findViewById(R.id.obra7);
        obra7.setText("Manual de moda do poeta");
        obra7.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra7.setTypeface(p1);
        obra7.setTextSize(18);


        autorObra7 = (TextView) rootView.findViewById(R.id.autorObra7);
        autorObra7.setText("Thiago Scarlata");
        autorObra7.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra7.setTypeface(p2);
        autorObra7.setTextSize(15);

        numeroObra7 = (TextView) rootView.findViewById(R.id.numeroObra7);
        numeroObra7.setText("20");
        numeroObra7.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra7.setTypeface(p1);
        numeroObra7.setTextSize(20);


        obra8 = (TextView) rootView.findViewById(R.id.obra8);
        obra8.setText("No que expresso, estou");
        obra8.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        obra8.setTypeface(p1);
        obra8.setTextSize(18);


        autorObra8 = (TextView) rootView.findViewById(R.id.autorObra8);
        autorObra8.setText("Olidnéri Bello");
        autorObra8.setTextColor(rootView.getResources().getColor(R.color.vermelhoEmarrom));
        autorObra8.setTypeface(p2);
        autorObra8.setTextSize(15);

        numeroObra8 = (TextView) rootView.findViewById(R.id.numeroObra8);
        numeroObra8.setText("22");
        numeroObra8.setTextColor(rootView.getResources().getColor(R.color.preto));
        numeroObra8.setTypeface(p1);
        numeroObra8.setTextSize(20);

    }

public void irObra1 (View v){

}
}
