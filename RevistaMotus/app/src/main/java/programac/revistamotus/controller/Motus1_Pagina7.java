package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import programac.revistamotus.R;
import programac.revistamotus.model.Motus1;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina7 extends Fragment{
    RelativeLayout layoutPagina7;
    TextView tituloTexto, texto, biografia, nome, pagina;
    private View rootView;
    Bundle bundle;
    int size=18;
    Motus1 m1 = new Motus1();


    public Motus1_Pagina7(){}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.pagina7, container, false);

        bundle = getArguments();
        if (bundle.getInt("size") != 0){
           size = bundle.getInt("size");
        }else {
            size = 18;
        }


        tituloTexto = (JustifiedTextView) rootView.findViewById(R.id.TitulodoTexto);
        Typeface p2 = Typeface.createFromAsset(this.getResources().getAssets(), "fonts/master_of_break.ttf");
        tituloTexto.setTypeface(p2);
        tituloTexto.setText("  Flores amarelas, esgoto letrado");
        tituloTexto.setTextColor(getResources().getColor(R.color.rosaclaro));
        tituloTexto.setTextSize(18);

        layoutPagina7 = (RelativeLayout) rootView.findViewById(R.id.layout7);
        layoutPagina7.setBackgroundColor(getResources().getColor(R.color.rosaclaro));

        texto = (JustifiedTextView) rootView.findViewById(R.id.Texto);
        Typeface p1 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/DroidSerif-Bold.ttf");
        texto.setTypeface(p1);
        texto.setText(m1.getTexto1());
        texto.setTextColor(getResources().getColor(R.color.preto));
        texto.setTextSize(size);

        biografia = (JustifiedTextView) rootView.findViewById(R.id.biografia);
        biografia.setTypeface(p1);
        biografia.setText(m1.getBiografiaTexto1());
        biografia.setTextColor(getResources().getColor(R.color.preto));
        biografia.setTextSize(size);


        nome = (JustifiedTextView) rootView.findViewById(R.id.nome);
        nome.setTypeface(p2);
        nome.setText(m1.getNomeAutor1());
        nome.setTextColor(getResources().getColor(R.color.preto));
        nome.setTextSize(size);

        pagina = (JustifiedTextView) rootView.findViewById(R.id.pagina);
        pagina.setTypeface(p2);
        pagina.setText(m1.getPagina7()+"  ");
        pagina.setTextColor(getResources().getColor(R.color.preto));
        pagina.setTextSize(size);
        return rootView;

    }





}
