package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import programac.revistamotus.R;
import programac.revistamotus.model.Motus1;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina3 extends Fragment {

    public View rootView;
    TextView informacao;
    TextView pagina;
    Bundle bundle;
    int size=18;
    Motus1 m1 = new Motus1();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.equipe, container, false);
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle.getInt("size") != 0){
            size = bundle.getInt("size");
        }else {
            size = 18;
        }

        Typeface p2 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/master_of_break.ttf");
        Typeface p3 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/BebasNeue.otf");

        pagina = (TextView) rootView.findViewById(R.id.pagina3);
        pagina.setTypeface(p2);
        pagina.setText(m1.getPagina3() + "  ");
        pagina.setTextSize(size);
        pagina.setTextColor(getResources().getColor(R.color.branco));

        informacao = (TextView) rootView.findViewById(R.id.informacao);
        informacao.setTypeface(p3);
        informacao.setText(m1.getEquipe());
        informacao.setTextSize(size);

        return  rootView;
    }
}


