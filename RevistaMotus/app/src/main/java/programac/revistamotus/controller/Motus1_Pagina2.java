package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import programac.revistamotus.R;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina2 extends Fragment {

    public View rootView;
    TextView motus, subtitulo;
    RelativeLayout layoutContraCapa;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.contracapa, container, false);
        super.onCreate(savedInstanceState);

        motus = (TextView) rootView.findViewById(R.id.motusContraCapa);
        Typeface p2 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/master_of_break.ttf");
        motus.setTypeface(p2);
        motus.setText("motus ");
        subtitulo = (TextView) rootView.findViewById(R.id.subtituloContraCapa);
        Typeface p1 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/BebasNeue.otf");
        subtitulo.setTypeface(p1);
        return  rootView;
    }
}

