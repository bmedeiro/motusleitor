package programac.revistamotus.controller;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import programac.revistamotus.R;

public class Sobre extends AppCompatActivity {
    TextView tituloPagina,textoSobre, temas, introTemas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sobre);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSobre);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif-Bold.ttf");
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif.ttf");



        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new NavigationView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Configuracao.class);
                startActivity(i);
            }
        });

        tituloPagina= (TextView) findViewById(R.id.TituloPagina);
        tituloPagina.setText(" SOBRE");
        tituloPagina.setTextColor(getResources().getColor(R.color.almostBlack));
        tituloPagina.setTypeface(p1);
        tituloPagina.setTextSize(22);



        textoSobre = (JustifiedTextView) findViewById(R.id.introSobre);
        textoSobre.setText("A Motus é uma ação de extensão produzida pela "+ Html.fromHtml("<html> <a href=\"www.unipampa.edu.br/alegrete/\">Universidade Federal do Pampa (UNIPAMPA) - campus Alegrete </a>")+"dentro do Programa C."+"\n" +
                "Dentre os principais objetivos da Motus, destacam-se:\n" +
                "\n" +
                " I. Incentivar a produção de obras literárias junto à comunidade acadêmica da UNIPAMPA e de indivíduos de qualquer estado, residentes no Brasil.\n" +
                " II. Despertar o interesse pela literatura nos estudantes e cidadãos.\n" +
                " III. Selecionar e publicar obras literárias inéditas, em língua portuguesa.\n" +
                "\n");
        textoSobre.setMovementMethod(LinkMovementMethod.getInstance());
        textoSobre.setTypeface(p2);
        textoSobre.setTextSize(16);

        temas= (TextView) findViewById(R.id.Temas);
        temas.setText("  TEMAS");
        temas.setTextColor(getResources().getColor(R.color.almostBlack));
        temas.setTypeface(p1);
        temas.setTextSize(22);



        introTemas = (TextView) findViewById(R.id.introTemas);
        introTemas.setText(
                " 1ª Ed. (2017/02) - LIBERDADE DE EXPRESSÃO \n" +
                " 2ª Ed. (2018/02) - TODA FORMA DE AMAR \n" + "\n");
        introTemas.setTypeface(p2);
        introTemas.setTextSize(15);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.action_settings) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }}