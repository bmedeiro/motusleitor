package programac.revistamotus.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import programac.revistamotus.R;

/**
 * Created by Bruno on 1/4/2018.
 */

public class Fonte extends AppCompatActivity {
    int points;
    RadioGroup rg1;
    RadioButton rb1, rb2, rb3, rb4;
    SharedPreferences pref;
    TextView tituloPagina;
    TextView textoF;
    Button legendaButton;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fonte);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final  SharedPreferences.Editor editor = pref.edit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFonte);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif-Italic.ttf");
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif.ttf");
        Typeface p3 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif-Bold.ttf");




        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new NavigationView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),  Configuracao.class);
                startActivity(i);
            }
        });


        rb1 = (RadioButton) findViewById(R.id.vinte);
        rb1.setTypeface(p2);
        rb1.setTextSize(20);
        rb1.setTextColor(getResources().getColor(R.color.almostBlack));

        rb2 = (RadioButton) findViewById(R.id.vinteedois);
        rb2.setTypeface(p2);
        rb2.setTextSize(22);
        rb2.setTextColor(getResources().getColor(R.color.almostBlack));

        rb3 = (RadioButton) findViewById(R.id.vinteequatro);
        rb3.setTypeface(p2);
        rb3.setTextSize(24);
        rb3.setTextColor(getResources().getColor(R.color.almostBlack));

        rb4 = (RadioButton) findViewById(R.id.dezoito);
        rb4.setTypeface(p2);
        rb4.setTextSize(18);
        rb4.setTextColor(getResources().getColor(R.color.almostBlack));

        if (pref.getInt("p",0)==18){
            rb4.setChecked(true);
        }else if(pref.getInt("p",0)==20){
            rb1.setChecked(true);
        }else if(pref.getInt("p",0)==22){
            rb2.setChecked(true);
        }else{
            rb3.setChecked(true);
        }






        textoF = (TextView) findViewById(R.id.textoFonte);
        textoF.setText("Deseja aumentar ou diminuir a fonte dos textos?\n"+"Temos quatro opções para melhorar sua leitura!");
        textoF.setTypeface(p1);
        textoF.setTextSize(18);
        textoF.setTextColor(getResources().getColor(R.color.almostBlack));

        tituloPagina= (TextView) findViewById(R.id.TituloPagina);
        tituloPagina.setText(" CONFIGURAÇÕES DE FONTE");
        tituloPagina.setTextColor(getResources().getColor(R.color.almostBlack));
        tituloPagina.setTypeface(p3);
        tituloPagina.setTextSize(22);


        legendaButton= (Button) findViewById(R.id.legendaButton);
        legendaButton.setText("CONFIRMAR");
        legendaButton.setTextColor(getResources().getColor(R.color.almostWhite));
        legendaButton.setTypeface(p2);
        legendaButton.setTextSize(14);
        legendaButton.setOnTouchListener(new ButtonCustomizado());



        rg1 = (RadioGroup) findViewById(R.id.rg1);
        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int i) {
                switch (i) {
                    case R.id.dezoito:

                        points = 18;
                        editor.putInt("p", points); // Storing integer
                        editor.commit();
                        break;
                    case R.id.vinte:
                        points = 20;
                        editor.putInt("p", points); // Storing integer
                        editor.commit();
                        break;
                    case R.id.vinteedois:
                        points = 22;
                        editor.putInt("p", points); // Storing integer
                        editor.commit();
                        break;
                    case R.id.vinteequatro:
                        points = 24;
                        editor.putInt("p", points); // Storing integer
                        editor.commit();
                        break;

                }

            }
        });

    }


    public void enviarDados() {
        int aux =    pref.getInt("p",0);
        Intent i = new Intent(this, Motus_Tab.class);
        Bundle bundle = new Bundle();
        bundle.putInt("T", aux);
        i.putExtras(bundle);
        startActivity(i);

    }


    public void confirmar(View view) {
        enviarDados();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_settings) {
            Intent i = new Intent(this, Fonte.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}

