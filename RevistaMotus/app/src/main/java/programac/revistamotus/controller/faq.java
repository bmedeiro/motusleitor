package programac.revistamotus.controller;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import programac.revistamotus.R;

public class faq extends AppCompatActivity {
    TextView tituloPagina;
    TextView pergunta1, resposta1,
             pergunta2, resposta2,
             pergunta3, resposta3,
             pergunta4, resposta4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFAQ);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif-Bold.ttf");
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif.ttf");



        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new NavigationView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Configuracao.class);
                startActivity(i);
            }
        });

        tituloPagina= (TextView) findViewById(R.id.TituloPagina);
        tituloPagina.setText(" PERGUNTAS FREQUENTES");
        tituloPagina.setTextColor(getResources().getColor(R.color.almostBlack));
        tituloPagina.setTypeface(p1);
        tituloPagina.setTextSize(22);



        pergunta1 = (JustifiedTextView) findViewById(R.id.pergunta1);
        pergunta1.setText("Quem pode participar da Motus?");
        pergunta1.setTypeface(p1);
        pergunta1.setTextSize(16);
        pergunta1.setTextColor(getResources().getColor(R.color.corDeÊnfase));


        resposta1 = (JustifiedTextView) findViewById(R.id.resposta1);
        resposta1.setText("Pessoas de quaisquer idades, profissões, culturas e nacionalidades podem inscrever seus contos e poemas para a Motus."
                +"\n"+" O único requisito é que a obra literária seja escrita em Língua Portuguesa.");
        resposta1.setTypeface(p2);
        resposta1.setTextSize(16);

        pergunta2 = (JustifiedTextView) findViewById(R.id.pergunta2);
        pergunta2.setText("Qual a frequência da revista?");
        pergunta2.setTypeface(p1);
        pergunta2.setTextSize(16);
        pergunta2.setTextColor(getResources().getColor(R.color.corDeÊnfase));


        resposta2 = (JustifiedTextView) findViewById(R.id.resposta2);
        resposta2.setText("As edições da Motus ocorrem anualmente.");
        resposta2.setTypeface(p2);
        resposta2.setTextSize(16);

        pergunta3 = (JustifiedTextView) findViewById(R.id.pergunta3);
        pergunta3.setText("Qual o formato da revista?");
        pergunta3.setTypeface(p1);
        pergunta3.setTextSize(16);
        pergunta3.setTextColor(getResources().getColor(R.color.corDeÊnfase));


        resposta3 = (JustifiedTextView) findViewById(R.id.resposta3);
        resposta3.setText("O intuito da Motus é ser uma revista literária digital, possibilitando que as obras literárias selecionas possam ser lidas através de computadores, celulares e tablets.");
        resposta3.setTypeface(p2);
        resposta3.setTextSize(16);

        pergunta4 = (JustifiedTextView) findViewById(R.id.pergunta4);
        pergunta4.setText("Se eu for selecionado(a), o que eu ganho?");
        pergunta4.setTypeface(p1);
        pergunta4.setTextSize(16);
        pergunta4.setTextColor(getResources().getColor(R.color.corDeÊnfase));


        resposta4 = (JustifiedTextView) findViewById(R.id.resposta4);
        resposta4.setText("A revista Motus é uma ação de extensão desenvolvida na Universidade Federal do Pampa. Em função disso, todo trabalho desenvolvido para a criação da revista é voluntário. "
                +"\n"+"Os escritores e escritoras que tiverem suas obras selecionadas terão como premiação a diagramação gratuita da revista e o Certificado de Seleção de suas obras.");
        resposta4.setTypeface(p2);
        resposta4.setTextSize(16);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.action_settings) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }}