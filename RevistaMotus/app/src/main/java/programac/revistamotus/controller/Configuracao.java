package programac.revistamotus.controller;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import programac.revistamotus.R;

public class Configuracao extends AppCompatActivity {
    TextView titulo;
    TextView bloco1, bloco2, bloco3;
    TextView ação1, ação2, ação3, ação4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);


        Typeface p1 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif-Bold.ttf");
        Typeface p2 = Typeface.createFromAsset(getResources().getAssets(), "fonts/DroidSerif.ttf");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFonte);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);



        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new NavigationView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Motus_Tab.class);
                startActivity(i);
            }
        });



        titulo = (TextView) findViewById(R.id.configurações);
        titulo.setText(" CONFIGURAÇÕES");
        titulo.setTypeface(p2);
        titulo.setTextSize(22);
        titulo.setTextColor(getResources().getColor(R.color.almostBlack));
        bloco1 = (TextView) findViewById(R.id.nomeDoBloco1);
        bloco1.setText("SOBRE");
        bloco1.setTypeface(p1);
        bloco1.setTextSize(20);
        bloco1.setTextColor(getResources().getColor(R.color.corDeÊnfase));

        ação1 = (TextView) findViewById(R.id.nomeAção);
        ação1.setText("Conhecer a motus");
        ação1.setTypeface(p2);
        ação1.setTextSize(18);
        ação1.setTextColor(getResources().getColor(R.color.almostBlack));
        ação1.setOnTouchListener(new TextViewCustomizado());


        ação2 = (TextView) findViewById(R.id.nomeAção2);
        ação2.setText("Perguntas Frequentes");
        ação2.setTypeface(p2);
        ação2.setTextSize(18);
        ação2.setTextColor(getResources().getColor(R.color.almostBlack));
        ação2.setOnTouchListener(new TextViewCustomizado());



        bloco2 = (TextView) findViewById(R.id.nomeDoBloco2);
        bloco2.setText("FONTE");
        bloco2.setTypeface(p1);
        bloco2.setTextSize(20);
        bloco2.setTextColor(getResources().getColor(R.color.corDeÊnfase));


        ação3 = (TextView) findViewById(R.id.nomeAção3);
        ação3.setText("Mudar o tamanho da fonte");
        ação3.setTypeface(p2);
        ação3.setTextSize(18);
        ação3.setHint("Aumente o tamanho da fonte utilizada nas obras da motus!");
        ação3.setHighlightColor(getResources().getColor(R.color.vermelhoEmarrom));
        ação3.setTextColor(getResources().getColor(R.color.almostBlack));
        ação3.setOnTouchListener(new TextViewCustomizado());


        bloco3 = (TextView) findViewById(R.id.nomeDoBloco3);
        bloco3.setText("CONTATO");
        bloco3.setTypeface(p1);
        bloco3.setTextSize(20);
        bloco3.setTextColor(getResources().getColor(R.color.corDeÊnfase));


        ação4 = (TextView) findViewById(R.id.nomeAção4);
        ação4.setText("Entrar em contato");
        ação4.setTypeface(p2);
        ação4.setTextSize(18);
        ação4.setHint("Mande sua sugestão/elogio/crítica!");
        ação4.setTextColor(getResources().getColor(R.color.almostBlack));
        ação4.setOnTouchListener(new TextViewCustomizado());



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.action_settings) {
            finish();
        }

        if (item.getItemId() == R.drawable.ic_back) {  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
            startActivity(new Intent(this, Motus_Tab.class));  //O efeito ao ser pressionado do botão (no caso abre a activity)
            finish();  //Método para matar a activity e não deixa-lá indexada na pilhagem
        }
        return super.onOptionsItemSelected(item);

    }

    public void irFonte(View view) {
        startActivity(new Intent(this, Fonte.class));
    }

    public void irContato(View view) { startActivity(new Intent(this, Contato.class));}

    public void irSobre(View view) { startActivity(new Intent(this, Sobre.class));}

    public void irFAQ(View view) { startActivity(new Intent(this, faq.class));}

}
