package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import programac.revistamotus.R;
import programac.revistamotus.model.Motus1;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina11 extends Fragment {
    RelativeLayout layoutPagina7;
    TextView tituloTexto, texto, biografia, nome, pagina;
    ScrollView scroll;
    View divisora;
    Bundle bundle;
    int size;
    private View rootView;
    Motus1 m1 = new Motus1();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.pagina7, container, false);

        bundle = getArguments();
        if (bundle.getInt("size") != 0){
            size = bundle.getInt("size");
        }else {
            size = 18;
        }

        tituloTexto = (TextView) rootView.findViewById(R.id.TitulodoTexto);
        Typeface p2 = Typeface.createFromAsset(this.getResources().getAssets(), "fonts/master_of_break.ttf");
        tituloTexto.setTypeface(p2);
        tituloTexto.setText("  Doma tua língua!");
        tituloTexto.setTextColor(getResources().getColor(R.color.queimado));
        tituloTexto.setTextSize(18);

        layoutPagina7 = (RelativeLayout) rootView.findViewById(R.id.layout7);
        layoutPagina7.setBackgroundColor(getResources().getColor(R.color.queimado));

        texto = (JustifiedTextView) rootView.findViewById(R.id.Texto);
        Typeface p1 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/BebasNeue.otf");
        texto.setTypeface(p1);
        texto.setText(m1.getTexto3());
        texto.setTextColor(getResources().getColor(R.color.preto));
        texto.setTextSize(size);

        biografia = (JustifiedTextView) rootView.findViewById(R.id.biografia);
        biografia.setTypeface(p1);
        biografia.setText(m1.getBiografiaTexto3());
        biografia.setTextColor(getResources().getColor(R.color.preto));
        biografia.setTextSize(size);


        nome = (JustifiedTextView) rootView.findViewById(R.id.nome);
        nome.setTypeface(p2);
        nome.setText(m1.getNomeAutor3());
        nome.setTextColor(getResources().getColor(R.color.preto));
        nome.setTextSize(size);

        pagina = (JustifiedTextView) rootView.findViewById(R.id.pagina);
        pagina.setTypeface(p2);
        pagina.setText(m1.getPagina11()+"  ");
        pagina.setTextColor(getResources().getColor(R.color.preto));
        pagina.setTextSize(size);


        divisora = (View) rootView.findViewById(R.id.divisoraTitulo);
        divisora.setBackgroundColor(getResources().getColor(R.color.preto));

        scroll = (ScrollView) rootView.findViewById(R.id.scrollObra);
        scroll.setBackgroundColor(getResources().getColor(R.color.queimado));
        return rootView;

    }


}
