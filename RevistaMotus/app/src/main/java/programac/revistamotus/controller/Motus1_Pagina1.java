package programac.revistamotus.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import programac.revistamotus.R;

/**
 * Created by Bruno on 1/3/2018.
 */

public class Motus1_Pagina1 extends Fragment implements View.OnClickListener {

    public View rootView;
    RelativeLayout layoutCapa;
    TextView titulo;
    Button next;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.capa, container, false);
        super.onCreate(savedInstanceState);

        titulo = (TextView) rootView.findViewById(R.id.titulo);
        Typeface p2 = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/master_of_break.ttf");
        titulo.setTypeface(p2);
        titulo.setText("motus  ");



        return rootView;


    }


    @Override
    public void onClick(View v) {
        Motus1_Pagina2 m = new Motus1_Pagina2();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.layoutPagina6, new Motus1_Pagina6());
        fragmentTransaction.commit();
    }

}

