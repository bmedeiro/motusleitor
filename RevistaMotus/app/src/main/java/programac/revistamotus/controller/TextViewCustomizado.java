package programac.revistamotus.controller;

import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Bruno on 2/1/2018.
 */

public class TextViewCustomizado implements View.OnTouchListener {
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch(motionEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                ((TextView)view).setTextColor(0xFF995522); //white
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                ((TextView)view).setTextColor(0xFF000000); //black
                break;
        }
        return false;
    }


}